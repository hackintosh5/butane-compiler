import os

from jinja2 import Environment, BaseLoader
from jinja2.exceptions import TemplateNotFound
from .paths import PathContext


class FileLoader(BaseLoader):
    def get_source(self, environment: "Environment", template: str):
        try:
            with open(template) as f:
                contents = f.read()

            mtime = os.path.getmtime(template)
            def uptodate() -> bool:
                try:
                    return os.path.getmtime(template) == mtime
                except OSError:
                    return False

            return (contents, template, uptodate)
        except FileNotFoundError:
            raise TemplateNotFound(template)

