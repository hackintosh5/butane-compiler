import shutil
import subprocess
import os
import yaml
from . import compile, merger

try:
    shutil.rmtree("output")
except FileNotFoundError:
    pass
compile.compile_modules(".", "modules", "setup", "templates", "files", "output")
configs = []
for entry in os.scandir("output"):
    assert entry.is_dir()
    config = os.path.join(entry.path, "config.bu")
    if os.path.isfile(config):
        configs.append(config)
merged_config = merger.merge_yamls(configs)
print(merged_config)
with open("config.bu", "w") as config_file:
    yaml.dump(merged_config, config_file)
subprocess.run(["butane", "--strict", "--output", "output.ign", "--files-dir", "output", "--pretty", "config.bu"], check=True)
