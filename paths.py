import os.path
from jinja2 import contextfunction

class PathContext:
    def __init__(self, root, modules, module, templates):
        self._root = root
        self._modules = os.path.join(root, modules)
        self._default_module = module
        self._templates = templates

    @contextfunction
    def _module_name(self, context):
        if context is None:
            return self._default_module
        head = os.path.relpath(context.name, self._modules)
        if head.startswith("../"):
            raise ValueError("Can't use module-sensitive paths when not in a module")
        tail = "."
        while head and tail:
            head, tail = os.path.split(head)
        return head or tail

    def _base(self, context):
        return os.path.join(self._modules, self._module_name(context), self._templates)

    def resolve_input_path(self, path, context):
        """
        `~/a` resolves to a sibling of the modules directory
        `../a` resolves to a child of the modules directory
        `./a` resolves to a child of the current module's directory
        `a` resolves to a child of the current module's template directory, which isn't guaranteed to be a sibling of the template
        """
        if "/../" in path + "/":
            # escaping is naughty but do we care?
            print("WARNING: escape detected")
        if path.startswith("~/"):
            return os.path.join(self._root, *(path[2:].split("/")))
        if path.startswith("./"):
            # technically doesn't need special casing...
            return os.path.join(self._base(context), *(path[2:].split("/")))
        if path.startswith("../"):
            module, *rest = path[3:].split("/")
            return os.path.join(self._modules, module, self._templates, *rest)
        return os.path.join(self._base(context), *(path.split("/")))

    def resolve_output_path(self, path, context):
        """
        `~/a` resolves to a sibling of the modules directory
        `../a` resolves to a child of the modules directory
        `./a` resolves to a child of the current module's directory
        `a` resolves to a child of the current module's template directory, which isn't guaranteed to be a sibling of the template
        """
        if "/../" in path + "/":
            # escaping is naughty but do we care?
            print("WARNING: escape detected")
        if path.startswith("~/"):
            return os.path.join(*(path[2:].split("/")))
        if path.startswith("./"):
            # technically doesn't need special casing...
            return os.path.join(self._module_name(context), *(path[2:].split("/")))
        if path.startswith("../"):
            # equivalent to ~
            module, *rest = path[3:].split("/")
            return os.path.join(module, *rest)
        return os.path.join(self._module_name(context), *(path.split("/")))
