def escape_json(s):
    return '"' + escape_quotes(s) + '"'

def escape_quotes(s):
    return s.replace("\\", "\\\\").replace('"', '\\"')
