import yaml


def merge_yamls(files):
    result = {}
    for path in files:
        with open(path) as file:
            data = yaml.unsafe_load(file)
        _merge_dict(result, data)
    return result

def _merge_dict(result, data):
    for key in data:
        if key in result:
            if isinstance(data[key], dict):
                if not isinstance(result[key], dict):
                    raise ValueError("Mismatched override")  # TODO log more detail
                _merge_dict(result[key], data[key])
            elif isinstance(data[key], list):
                if not isinstance(result[key], list):
                    raise ValueError("Mismatched override")  # TODO log more detail
                result[key] += data[key]
            elif result[key] != data[key]:
                raise ValueError("Mismatched override")  # TODO log more detail
        else:
            result[key] = data[key]
