import builtins
import os.path
import shutil
from jinja2 import Environment, select_autoescape, StrictUndefined, contextfunction

from .file_loader import FileLoader
from .paths import PathContext
from .escapers import escape_json, escape_quotes

MODULE_ROOT = "modules"
OUTPUT = "output"
TEMPLATES = "templates"
STATIC_FILES = "files"


def compile_module(module, root, modules, setup, templates, static_files, output):
    module_base = os.path.join(root, modules, module.name)
    module_setup = os.path.join(module_base, setup)
    module_templates = os.path.join(module_base, templates)
    module_files = os.path.join(module_base, static_files)

    path_context = PathContext(root, modules, module.name, templates)
    env = Environment(
        loader=FileLoader(),
        autoescape=False,
        auto_reload=False,
        undefined=StrictUndefined
    )
    env.filters["ej"] = env.filters["escape_json"] = escape_json
    env.filters["ey"] = env.filters["escape_yaml"] = escape_json
    env.filters["eq"] = env.filters["escape_quotes"] = escape_quotes

    @contextfunction
    def resolve_input_path(context, path):
        return path_context.resolve_input_path(path, context)

    @contextfunction
    def resolve_output_path(context, path):
        return path_context.resolve_output_path(path, context)

    env.filters["rip"] = env.globals["resolve_input_path"] = resolve_input_path
    env.filters["rop"] = env.globals["resolve_output_path"] = resolve_output_path
    env.globals["builtins"] = builtins

    def static_include(path):
        with open(path) as file:
            return file.read()

    env.globals["static_include"] = static_include

    if os.path.isdir(module_setup):
        for entry in os.scandir(module_setup):
            if entry.is_file():
                with open(entry.path) as file:
                    exec(file.read())

    os.makedirs(os.path.join(OUTPUT, module.name))

    if os.path.isdir(module_templates):
        for entry, entry_path, entry_output in walk2(module.name, module_templates, output):
            template = env.get_template(os.path.join(module_templates, *entry_path[1:]))
            template.stream().dump(open(entry_output, "w"))

    if os.path.isdir(module_files):
        for entry, _, entry_output in walk2(module.name, module_files, output):
            shutil.copy(entry.path, entry_output, follow_symlinks=False)


def walk2(base_name, base_dir, output):
    stack = [(base_name, os.scandir(base_dir))]
    while stack:
        entry = next(stack[-1][1], None)
        if not entry:
            stack.pop()
            continue
        entry_path = [*(name for name, _ in stack), entry.name]
        entry_output = os.path.join(output, *entry_path)
        if entry.is_dir():
            if os.path.exists(entry_output):
                if not os.path.isdir(entry_output):
                    raise ValueError(f"{entry_output} exists and isn't directory while creating dir {entry.path}")
            else:
                os.mkdir(entry_output)
            stack.append((entry.name, os.scandir(entry.path)))
        elif entry.is_file():
            if os.path.lexists(entry_output):
                raise ValueError(f"{entry_output} exists while creating file {entry.path}")
            yield entry, entry_path, entry_output
        else:
            raise TypeError(f"Unknown file type for {entry.path}")


def compile_modules(root, modules, setup, templates, static_files, output):
    with os.scandir(os.path.join(root, modules)) as modules_iter:
        for module in modules_iter:
            compile_module(module, root, modules, setup, templates, static_files, output)

